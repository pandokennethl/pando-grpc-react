This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# TL;DR
Please don't mind 

## Naming convention

* `lowerCamelCase` variable / parameter / function / method / property / module alias
* `UpperCamelCase` class / interface / type / enum / decorator / type parameters
* `CONSTANT_CASE` global constant values, including enum values
* Interface no I prefix `interface IApplicationConfig = {} // please don't do this`
* Declarative names

## Typescript

* Functional programming and functional component because why not?, because it's cool, because we can
* NO any data type as much as you can `type Foo = { bar: any }`

## Folder structure

```
|-- src
   |-- client
   |   |-- pages
   |   |-- application
   |   |-- atomic   
   |   |-- global-components
   |   |-- global-styles
   |   |-- global-state / redux ?
   |   |-- utils
   |   |-- system
   |   |   |-- bootloader
   |-- server
```

## State Management
### Prospects
 * Redux + Saga
 * XState + Context Api

## Git
### Branch naming

* bugfix/TICKET-NUMBER `bugfix/TPV-123`
* feature/TICKET-NUMBER `feature/TPV-221` or feature/ShortDescription `feature/AddSomeThings`
* release/date `release/2022.01.02`

Commit messages needs to descriptive `Added some things to fix other things`

## SSR
### Prospect
* Razzle