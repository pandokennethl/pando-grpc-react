import { SurveyAdminServiceClient } from "@pandolink/grpcweb/src/pando/api/survey/v1/admin_grpc_web_pb";
import { GetSurveysRequest } from "@pandolink/grpcweb/src/pando/api/survey/v1/admin_pb";
import { setRequest } from "@pandolink/utils";

interface GetSurveyRequestParams extends Partial<GetSurveysRequest.AsObject> {
  organizationCode: string;
}

const getToken = async () => {
  const tokenHost = process.env.REACT_APP_TOKEN_HOST ?? ''
  const username = process.env.REACT_APP_USERNAME ?? ''
  const password = process.env.REACT_APP_PASSWORD ?? ''
  const clientId = process.env.REACT_APP_CLIENT_ID ?? ''
  const clientSecret = process.env.REACT_APP_CLIENT_SECRET ?? ''
  const grantType = process.env.REACT_APP_GRANT_TYPE ?? ''
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    body: new URLSearchParams({
      username,
      password,
      client_id: clientId,
      client_secret: clientSecret,
      grant_type: grantType
    })
  }

  const res = await fetch(tokenHost, requestOptions)
  const resJson = await res.json()

  return resJson.access_token
}

export const getSurveys = async (params: GetSurveyRequestParams) => {
  const host = process.env.REACT_APP_HOST
  const port = process.env.REACT_APP_PORT
  const accessToken = await getToken()
  const surveyAdminService = new SurveyAdminServiceClient(
    `${host}:${port}`,
  )
  const request = setRequest(new GetSurveysRequest(), params)

  const call = surveyAdminService.getSurveys(
    request,
    {
      'authorization': `Bearer ${accessToken}`
    },
    (err, response) => {
      console.log('response', response ? response.toObject() : response)
    }
  )

  call.on('status', (status) => {
    console.log('surveyAdminService service:', status)
  })
}