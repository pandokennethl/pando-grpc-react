import {getSurveys} from "../utils/util";

const Surveys = () => {
  getSurveys({
    organizationCode: 'alliance',
    resultsPerPage: 2
  })

  return (
    <></>
  )
}

export default Surveys;