import React from 'react';
import './App.css';
import Surveys from "./components/Surveys";

function App() {
  return (
    <div className="App">
      <p>Open console logs</p>
      <Surveys />
    </div>
  );
}

export default App;
